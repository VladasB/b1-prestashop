Įskiepis skirtas sinchronizuoti produktus tarp Prestashop ir B1.lt aplikacijos.

### Reikalavimai ###

* Prestashop 1.6, 1.7
* PHP 5.5

### Diegimas ###

* `NEBŪTINA` Pasidarykite failų atsarginę kopiją.
* Padarykite atsarginę DB kopiją.
* Perkelkite `b1accounting` direktoriją į Prestashop `modules` direktoriją.
* Administracijos valdymo pulte nueikite į 'Modules and Services' skiltį.
* Esant 'Modules and Services' puslapyje, paieškos laukelyje įveskite 'b1' ir žemiau sąraše atsiras B1 modulis, šalia kurio reikia paspausti 'Install'
* Po modulio įdiegimo pakartokite šios žingsnius, tik jau bus ne 'Install', o 'Configurate'.
* Paspaudus 'Configurate' mygtuką atsidarys modulio konfigūracinis puslapis, kuriame reikia suvesti reikalingą informaciją.
* Konfigūracijos puslapyje 'Orders sync from' laukelyje nurodykite data, nuo kurios bus sinchromnizuojami užsakymai. Datos formatas Y-m-d. Pvz. 2016-12-10 
* Konfigūracijos puslapyje 'Confirmed order status ID' laukelyje nurodykite užsakymų, kurie bus sinchronizuojami su B1, statuso ID. Pagal nutylėjimą 2(Completed).
* Konfigūracijos puslapyje 'Sync quantities' laukelyje, pasirinkite ar bus sinchromnizuojami kiekiai.
* Konfigūracijos puslapyje 'VAT Tax rate' laukelyje, nurodykite taikomą PVM mokestį (0%, 9%, 21%).
* Prie serverio Cron darbų sąrašo pridėkite visus išvardintus cron darbus, nurodytus modulio konfigūravimo puslapyje.
  Pridėti cron darbus galite per serverio valdymo panelė (DirectAdmin, Cpanel) arba įvykdę šias komandinės eilutės serverio pusėje  
    * `0 */12 * * * wget -q -O - '[products_cron_url]'` Vietoj [products_cron_url] reikia nurodyti savo Cron adresą. 
    * `*/5 * * * * wget -q -O - '[orders_cron_url]'` Vietoj [orders_cron_url] reikia nurodyti savo Cron adresą.  
* Įvykdykite cron užklausas spausdami `Sync orders`, `Sync products`.


### Kontaktai ###

* Kilus klausimams, prašome kreiptis info@b1.lt