<?php

if (!defined('_PS_VERSION_'))
    exit;

require_once _PS_MODULE_DIR_ . 'b1accounting/libraries/B1.php';
require_once _PS_MODULE_DIR_ . 'b1accounting/models/items.php';
require_once _PS_MODULE_DIR_ . 'b1accounting/models/orders.php';

class B1Accounting extends Module
{

    public function __construct()
    {
        $this->name = 'b1accounting';
        $this->tab = 'administration';
        $this->version = '1.4.3';
        $this->author = 'B1.lt';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('B1.lt accounting module');
        $this->description = $this->l('B1.lt accounting module');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('b1accounting'))
            $this->warning = $this->l('No name provided');
    }

    public function getContent()
    {
        if (Tools::isSubmit('submitModule')) {
            Configuration::updateValue('b1_api_key', Tools::getValue('b1_api_key'));
            Configuration::updateValue('quantity_sync', Tools::getValue('quantity_sync'));
            Configuration::updateValue('tax_rate', Tools::getValue('tax_rate'));
            Configuration::updateValue('orders_sync_from', Tools::getValue('orders_sync_from'));
            Configuration::updateValue('order_status_id', Tools::getValue('order_status_id'));
            Configuration::updateValue('b1_private_key', Tools::getValue('b1_private_key'));
            Configuration::updateValue('b1_shop_id', Tools::getValue('b1_shop_id'));
            Configuration::updateValue('b1_cron_key', Tools::getValue('b1_cron_key'));
            Tools::redirectAdmin('index.php?tab=AdminModules&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
        }
        $this->context->smarty->assign('settings_contact_email', 'info@b1.lt');
        $this->context->smarty->assign('settings_documentation_url', 'https://www.b1.lt/doc/api');
        $this->context->smarty->assign('settings_help_page_url', 'https://www.b1.lt/help');

        return $this->renderForm() . $this->display(__FILE__, 'views/templates/admin/content.tpl');
    }

    public function renderForm()
    {
        $states = ModelB1Orders::getOrderStates();
        $states_html = '';
        foreach ($states as $item) {
            $states_html .= $item['id_order_state'] . ' - ' . $item['name'] . '<br>';
        }

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Shop unique identifier'),
                        'name' => 'b1_shop_id',
                        'desc' => $this->l('Custom shop unique identifier for shop identification'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Orders sync from'),
                        'name' => 'orders_sync_from',
                        'desc' => $this->l('Date format: Y-m-d. Example: 2016-10-12 '),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Confirmed order status ID'),
                        'name' => 'order_status_id',
                        'desc' => $this->l('Please select that ID, which orders will be sync') . '<br>' . $states_html,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Sync quantity'),
                        'name' => 'quantity_sync',
                        'desc' => $this->l('Enable sync quantity'),
                        'options' => [
                            'query' => [
                                [
                                    'id' => '0',
                                    'name' => 'Disable',
                                ],
                                [
                                    'id' => '1',
                                    'name' => 'Enable',
                                ]
                            ],
                            'id' => 'id',
                            'name' => 'name'
                        ],
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('VAT Tax rate'),
                        'name' => 'tax_rate',
                        'desc' => $this->l('If in your shop enabled VAT please select that  tax ID. If not leave empty'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('API key'),
                        'name' => 'b1_api_key',
                        'desc' => $this->l('B1.lt API key'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Private key'),
                        'name' => 'b1_private_key',
                        'desc' => $this->l('B1.lt private API key'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Cron key'),
                        'name' => 'b1_cron_key',
                        'desc' => $this->l('Cron key for security'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Cron url products'),
                        'name' => 'b1_cron_url_products',
                        'desc' => $this->l('Please add this URL to server Cron jobs list or use Cron services (easycron, setcronjob). Recommended update once per week.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Cron url products'),
                        'name' => 'b1_cron_url_update_products',
                        'desc' => $this->l('Please add this URL to server Cron jobs list or use Cron services (easycron, setcronjob). Recommended update once per day.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Cron url orders'),
                        'name' => 'b1_cron_url_orders',
                        'desc' => $this->l('Please add this URL to server Cron jobs list or use Cron services (easycron, setcronjob). Recommended update every 10 minutes.'),
                    ),
                    array(
                        'type' => '',
                        'name' => '',
                        'label' => $this->l('Run crons'),
                        'desc' => '<a class="btn btn-primary btn-xs" taget="_blank" href="' . $this->getConfigFieldsValues()['b1_cron_url_products'] . '">' . $this->l('Sync products') . '</a> ' .
                            '<a class="btn btn-primary btn-xs" taget="_blank" href=" ' . $this->getConfigFieldsValues()['b1_cron_url_orders'] . '">' . $this->l('Sync orders') . '</a>' .
                            ' <a class="btn btn-primary btn-xs" taget="_blank" href=" ' . $this->getConfigFieldsValues()['b1_cron_url_update_products'] . '">' . $this->l('Update products') . '</a>',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $cron_url = _PS_BASE_URL_ . __PS_BASE_URI__ . 'modules/b1accounting/cron.php';

        return array(
            'b1_api_key' => Tools::getValue('b1_api_key', Configuration::get('b1_api_key')),
            'quantity_sync' => Tools::getValue('quantity_sync', Configuration::get('quantity_sync')),
            'tax_rate' => Tools::getValue('tax_rate', Configuration::get('tax_rate')),
            'b1_private_key' => Tools::getValue('b1_private_key', Configuration::get('b1_private_key')),
            'orders_sync_from' => Tools::getValue('orders_sync_from', Configuration::get('orders_sync_from')),
            'order_status_id' => Tools::getValue('order_status_id', Configuration::get('order_status_id')),
            'b1_shop_id' => Tools::getValue('b1_shop_id', Configuration::get('b1_shop_id')),
            'b1_cron_key' => Tools::getValue('b1_cron_key', Configuration::get('b1_cron_key')),
            'b1_cron_url' => $cron_url,
            'b1_cron_url_products' => $cron_url . '?id=products&key=' . Configuration::get('b1_cron_key'),
            'b1_cron_url_update_products' => $cron_url . '?id=updateproducts&key=' . Configuration::get('b1_cron_key'),
            'b1_cron_url_orders' => $cron_url . '?id=orders&key=' . Configuration::get('b1_cron_key'),
        );
    }

    public function install()
    {
        Configuration::updateValue('b1_initial_sync', 0);
        Configuration::updateValue('b1_shop_id', substr(md5(time()), 0, 5));
        Configuration::updateValue('b1_cron_key', md5(time()));
        Configuration::updateValue('orders_sync_from', date('Y-m-d'));
        Configuration::updateValue('initial_product_sync_done', 0);
        Configuration::updateValue('latest_product_sync_date', date('Y-m-d'));

        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'orders` ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL'
        );
        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'product_attribute` ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL'
        );
        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'product` ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL'
        );
        if (!parent::install())
            return false;
        return true;
    }

    public function uninstall()
    {
        Configuration::deleteByName('b1_initial_sync');
        Configuration::deleteByName('b1_cron_key');
        Configuration::deleteByName('b1_shop_id');
        Configuration::deleteByName('orders_sync_from');
        Configuration::deleteByName('b1_api_key');
        Configuration::deleteByName('b1_private_key');
        Configuration::deleteByName('order_status_id');
        Configuration::deleteByName('tax_rate');
        Configuration::deleteByName('quantity_sync');
        Configuration::deleteByName('initial_product_sync_done');
        Configuration::deleteByName('latest_product_sync_date');

        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'orders` DROP COLUMN `b1_reference_id`'
        );
        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'product_attribute` DROP COLUMN `b1_reference_id`'
        );
        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'product` DROP COLUMN `b1_reference_id`'
        );

        if (!parent::uninstall())
            return false;
        return true;
    }

}
