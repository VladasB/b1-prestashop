<?php

require('../../config/config.inc.php');
require_once('libraries/B1.php');
require_once('models/orders.php');
require_once('models/items.php');
require_once('models/helper.php');

class B1CronException extends Exception
{

    private $extraData;

    public function __construct($message = "", $extraData = array(), $code = 0, \Exception $previous = null)
    {
        $this->extraData = $extraData;
        parent::__construct($message, $code, $previous);
    }

    public function getExtraData()
    {
        return $this->extraData;
    }

}

class B1Cron extends Module
{

    const TTL = 3600;
    const MAX_ITERATIONS = 100;
    const ORDERS_PER_ITERATION = 100;
    const ORDER_SYNC_THRESHOLD = 10;

    private $library;

    private function init()
    {
        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);
    }

    private function validateAccess()
    {
        $cron_key = Configuration::get('b1_cron_key');
        if ($cron_key == null) {
            throw new B1CronException('Fatal error.');
        }

        $get_key = Tools::getValue('key');
        if ($get_key != $cron_key) {
            exit();
        }
    }

    public function __construct()
    {
        $this->init();
        $this->validateAccess();

        $get_id = Tools::getValue('id');
        $this->library = new B1([
            'apiKey' => Tools::getValue('b1_api_key', Configuration::get('b1_api_key')),
            'privateKey' => Tools::getValue('b1_private_key', Configuration::get('b1_private_key'))
        ]);

        switch ($get_id) {
            case 'products':
                $this->fetchProducts();
                break;
            case 'updateproducts':
                $this->fetchProducts(false);
                break;
            case 'orders':
                $this->syncOrders();
                break;
            default:
                throw new Exception('Bad action ID specified.');
        }
    }

    private function fetchProducts($syncAll = true)
    {
        $i = 0;
        $lid = null;
        $modAfterDate = $syncAll ? null : Configuration::get('latest_product_sync_date');
        $enableQuantitySync = Tools::getValue('quantity_sync', Configuration::get('quantity_sync'));
        if (!$enableQuantitySync) {
            throw new Exception('Not set quantity_sync value');
        }
        if ($syncAll) {
            ModelB1Items::resetAllB1ReferenceId();
        }
        do {
            ob_start();
            $i++;
            $response = $this->library->request('shop/product/list', ["lid" => $lid, "lmod" => $modAfterDate]);
            $data = $response->getContent();
            foreach ($data['data'] as $item) {
                if ($item['code'] != NULL) {
                    ModelB1Items::updateProductReferenceId(pSQL($item['id']), pSQL($item['code']));
                    ModelB1Items::updateProductAttributeReferenceId(pSQL($item['id']), pSQL($item['code']));
                    if ($enableQuantitySync == 1) {
                        $productId = ModelB1Items::fetchProductIdByB1Id(pSQL($item['id']));
                        if ($productId == null) {
                            $attributeId = ModelB1Items::fetchProductAttributeIdByB1Id(pSQL($item['id']));
                            $productId = ModelB1Items::fetchProductVariationIdByB1Id($attributeId);
                            StockAvailableCore::setQuantity($productId, $attributeId, pSQL($item['quantity']));
                        } else {
                            StockAvailableCore::setQuantity($productId, 0, pSQL($item['quantity']));
                        }
                    }
                }
            }
            if (count($data['data']) == 100) {
                $lid = $data['data'][99]['id'];
            } else {
                $lid = -1;
            }
            echo "$i;";
            ob_end_flush();
        } while ($lid != -1 && $i < self::MAX_ITERATIONS);
        if ($syncAll) {
            Configuration::updateValue('initial_product_sync_done', 1);
        } else {
            Configuration::updateValue('latest_product_sync_date', date("Y-m-d"));
        }
        echo 'OK';
    }

    /**
     * @throws B1Exception|Exception
     */
    private function syncOrders()
    {
        $order_status_id = Tools::getValue('order_status_id', Configuration::get('order_status_id'));
        if (!$order_status_id) {
            throw new Exception('Not set order_status_id value');
        }
        $orders_sync_from = Tools::getValue('orders_sync_from', Configuration::get('orders_sync_from'));
        if (!$orders_sync_from) {
            throw new Exception('Not set orders_sync_from value');
        }
        $data_prefix = Tools::getValue('b1_shop_id', Configuration::get('b1_shop_id'));
        if (!$data_prefix) {
            throw new Exception('Not set shop_id value');
        }
        $enableQuantitySync = Tools::getValue('quantity_sync', Configuration::get('quantity_sync'));
        if (!$enableQuantitySync) {
            throw new Exception('Not set quantity_sync value');
        }
        $initial_sync = Tools::getValue('b1_initial_sync', Configuration::get('b1_initial_sync'));
        if ($initial_sync === false) {
            throw new Exception('Not set initial_sync value');
        }
        $initial_product_sync_done = Tools::getValue('initial_product_sync_done', Configuration::get('initial_product_sync_done'));
        if (!$initial_product_sync_done) {
            throw new Exception('Initial product sync is not done yet.');
        }
        $i = 0;
        do {
            ob_start();
            $i++;
            $orders = ModelB1Orders::getOrdersForSync(pSQL($order_status_id), pSQL($orders_sync_from), self::ORDERS_PER_ITERATION);
            $processed = 0;
            foreach ($orders as $item) {
                $order_data = $this->generateOrderData($item, $data_prefix, $initial_sync);
                try {
                    $response = $this->library->request('shop/order/add', $order_data['data']);
                    $request = $response->getContent();
                    ModelB1Orders::assignB1OrderId($request['data']['orderId'], $item['id_order']);
                    if ($enableQuantitySync == 1 && isset($request['data']['update'])) {
                        foreach ($request['data']['update'] as $productQuantity) {
                            if ((ModelB1Items::fetchProductIdByB1Id(pSQL($item['id']))) == null) {
                                $attributeId = ModelB1Items::fetchProductAttributeIdByB1Id(pSQL($item['id']));
                                $productId = ModelB1Items::fetchProductVariationIdByB1Id($attributeId);
                                StockAvailableCore::setQuantity($productId, $attributeId, pSQL($productQuantity['quantity']));
                            } else {
                                $productId = ModelB1Items::fetchProductIdByB1Id(pSQL($item['id']));
                                $attributeId = 0;
                                StockAvailableCore::setQuantity($productId, $attributeId, pSQL($item['quantity']));
                            }
                        }
                    }
                } catch (B1DuplicateException $e) {
                    $content = $e->getResponse()->getContent();
                    ModelB1Orders::assignB1OrderId($content['data']['orderId'], $item['id_order']);
                }
                $processed++;
                echo "$i-$processed;";
            }
            ob_end_flush();
        } while ($processed == self::ORDERS_PER_ITERATION && $i < self::MAX_ITERATIONS);
        if ($initial_sync == 0) {
            Configuration::updateValue('b1_initial_sync', 1);
        }
        echo 'OK';
    }

    private function generateOrderData($item, $data_prefix, $initial_sync)
    {
        $tax_rate = Configuration::get('tax_rate');
        $order = new Order($item['id_order']);
        $order_data = array();
        $order_data['prefix'] = $data_prefix;
        $order_data['writeOff'] = $initial_sync;
        $customer = new Customer($order->id_customer);
        $address_billing = new Address($order->id_address_invoice);
        $address_delivery = new Address($order->id_address_delivery);
        $country_billing = new Country($address_billing->id_country);
        $country_delivery = new Country($address_billing->id_country);
        $order_data['orderId'] = $order->id;
        $order_data['orderDate'] = substr($order->date_add, 0, 10);
        $order_data['orderNo'] = $order->reference;
        $order_data['currency'] = Currency::getCurrency($order->id_currency)['iso_code'];
        $order_data['discount'] = intval(round($order->total_discounts_tax_incl * 100));
        $order_data['total'] = intval(round($order->total_paid_tax_incl * 100));
        $order_data['orderEmail'] = $customer->email;
        $order_data['vatRate'] = intval(round($tax_rate));
        $order_data['shippingAmount'] = intval(round($order->total_shipping_tax_incl * 100));
        $order_data['billing']['name'] = $address_billing->company == '' ? trim($address_billing->firstname . ' ' . $address_billing->lastname) : $address_billing->company;
        $order_data['billing']['isCompany'] = $address_billing->company == '' ? 0 : 1;
        $order_data['billing']['address'] = $address_billing->address1;
        $order_data['billing']['city'] = $address_billing->city;
        $order_data['billing']['country'] = $country_billing->iso_code;
        $order_data['delivery']['name'] = $address_delivery->company == '' ? trim($address_delivery->firstname . ' ' . $address_delivery->lastname) : $address_delivery->company;
        $order_data['delivery']['isCompany'] = $address_delivery->company == '' ? 0 : 1;
        $order_data['delivery']['address'] = $address_delivery->address1;
        $order_data['delivery']['city'] = $address_delivery->city;
        $order_data['delivery']['country'] = $country_delivery->iso_code;
        if ($order_data['billing']['name'] == '') {
            $order_data['billing'] = $order_data['delivery'];
        }
        if ($order_data['delivery']['name'] == '') {
            $order_data['delivery'] = $order_data['billing'];
        }
        foreach (ModelB1Orders::getProductsDetail($item['id_order']) as $key => $product) {

            $order_data['items'][$key]['id'] = $product['b1_reference_id'];
            $order_data['items'][$key]['name'] = $product['product_name'];
            $order_data['items'][$key]['quantity'] = intval(round($product['product_quantity'], 2) * 100);
            $order_data['items'][$key]['price'] = intval(round($product['unit_price_tax_incl'], 2) * 100);
            $order_data['items'][$key]['sum'] = intval(round($product['total_price_tax_incl'], 2) * 100);
        }

        return [
            'data' => $order_data
        ];
    }
}

new B1Cron;
