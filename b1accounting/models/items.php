<?php

class ModelB1Items extends Module
{

    public static function resetAllB1ReferenceId()
    {
        \Db::getInstance()->execute("UPDATE " . _DB_PREFIX_ . "product SET b1_reference_id = NULL ");
        \Db::getInstance()->execute("UPDATE " . _DB_PREFIX_ . "product_attribute SET b1_reference_id = NULL ");
    }

    public static function updateProductReferenceId($id, $code)
    {
        \Db::getInstance()->execute("UPDATE " . _DB_PREFIX_ . "product SET b1_reference_id = " . pSQL($id) . " WHERE reference = '" . pSQL($code) . "'");
    }

    public static function updateProductAttributeReferenceId($id, $code)
    {
        \Db::getInstance()->execute("UPDATE " . _DB_PREFIX_ . "product_attribute SET b1_reference_id = " . pSQL($id) . " WHERE reference = '" . pSQL($code) . "'");
    }

    public static function fetchProductIdByB1Id($id)
    {
        return \Db::getInstance()->getValue("SELECT id_product FROM " . _DB_PREFIX_ . "product WHERE b1_reference_id = " . pSQL($id));
    }

    public static function fetchProductAttributeIdByB1Id($id)
    {
        return \Db::getInstance()->getValue("SELECT id_product_attribute FROM " . _DB_PREFIX_ . "product_attribute WHERE b1_reference_id = " . pSQL($id));
    }

    public static function fetchProductVariationIdByB1Id($variationId)
    {
        return \Db::getInstance()->getValue("SELECT id_product FROM " . _DB_PREFIX_ . "stock_available WHERE id_product_attribute = " . pSQL($variationId));
    }

}