<?php

class ModelB1Orders extends Module
{

    public static function getOrderStates()
    {
        $sql = "SELECT * FROM `" . _DB_PREFIX_ . "order_state_lang` WHERE id_lang = " . Configuration::get('PS_LANG_DEFAULT') . " ORDER BY id_order_state";
        return \Db::getInstance()->executeS($sql);
    }

    public static function getOrdersForSync($status, $date, $iteration)
    {
        return \Db::getInstance()->executeS("SELECT * FROM " . _DB_PREFIX_ . "orders WHERE (`current_state` = '" . pSQL($status) . "' AND `invoice_date` != '0000-00-00 00:00:00' AND `invoice_date` >= '" . pSQL($date) . "') AND b1_reference_id IS NULL ORDER BY invoice_date ASC LIMIT " . $iteration);
    }

    public static function assignB1OrderId($b1_order, $order_id)
    {
        \Db::getInstance()->query("UPDATE " . _DB_PREFIX_ . "orders SET b1_reference_id = " . pSQL($b1_order) . " WHERE `id_order` = " . pSQL($order_id));
    }

    public static function getProductsDetail($id)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT od.*,COALESCE(pa.b1_reference_id, p.b1_reference_id) AS b1_reference_id
		FROM `' . _DB_PREFIX_ . 'order_detail` od
		LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa ON (pa.id_product_attribute = od.product_attribute_id)
		LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON (p.id_product = od.product_id )
		LEFT JOIN `' . _DB_PREFIX_ . 'product_shop` ps ON (ps.id_product = p.id_product AND ps.id_shop = od.id_shop)
		WHERE od.`id_order` = ' . $id);
    }

}
