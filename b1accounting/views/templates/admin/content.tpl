<div class="panel">
    <div class="container-fluid">
        <div class="pull-right">
            <a href="mailto:{$settings_contact_email}" data-toggle="tooltip" title="{l s='Contact' mod='b1accounting'}" class="btn btn-default">
                <i class="icon-envelope"></i> <span class="hidden-xs">{l s='Contact' mod='b1accounting'}</span>
            </a>
            <a href="{$settings_documentation_url}" data-toggle="tooltip" title="{l s='B1 API documentation' mod='b1accounting'}" target="_blank" class="btn btn-default">
                <i class="icon-book"></i> <span class="hidden-xs">{l s='B1 API documentation' mod='b1accounting'}</span>
            </a>
            <a href="{$settings_help_page_url}" data-toggle="tooltip" title="{l s='B1 Help' mod='b1accounting'}" target="_blank" class="btn btn-default">
                <i class="icon-book"></i> <span class="hidden-xs">{l s='B1 Help' mod='b1accounting'}</span>
            </a>
        </div>
    </div>
</div>
