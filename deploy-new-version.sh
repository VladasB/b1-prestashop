#!/bin/bash

##########
# Deploy new version
##########

set -e # Exit on error
#set -x # See what is happening when executing the script.

# Make sure user specifies the new version
if [ $# -ne 1 ]; then
	echo "Please specify the new version."
	echo "Example: ./deploy-new-version.sh 1.2.6"
    exit 1
fi

git pull
sed -i "s/const VERSION = '.*';/const VERSION = '$1';/" b1accounting/libraries/B1.php
sed -i "s/const PLATFORM = '.*';/const PLATFORM = 'prestashop';/" b1accounting/libraries/B1.php
sed -i "s/this->version = '.*'/this->version = '$1'/" b1accounting/b1accounting.php
sed -i "s/<version>.*<\/version>/<version><![CDATA[$1]]><\/version>/" b1accounting/config_lt.xml
git commit -am "Bump plugin version"
git push
git tag -a $1 -m "New version"
git push --tags